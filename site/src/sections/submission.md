---
title: 🔮  Hidden Charms (Flags) & Final Submission
type: submission
---

Throughout this challenge, special charms (known as 'flags') are hidden within certain CI/CD pipeline jobs. Successfully completing a job will unveil its charm.{.font-light .text-black .text-xl}

</br>

<div class="flex justify-center items-center">
    <div class="text-justify text-2xl">
        <span class="font-bold text-halloween-cauldron">
            Your task:
        </span>
        <ol class="list-decimal pl-5 font-bold text-halloween-cauldron">
            <li>Collect all the charms as evidence of your mastery over each challenge.</li>
            <li>Deploy your FastAPI application to your chosen cloud platform.</li>
            <li>Submit to the provided link below as proof of deployment.</li>
        </ol>
    </div>
</div>

</br>

<br>

Under the watchful eyes of a thousand stars, Mighty Saver Rabbit waits. He believes in your magic and hopes you emerge victorious from this ghoulish escapade. Beware, for the clock is ticking, and Halloween draws near! {.font-light .text-black .text-xl}
